# RUB to PLN converter v.1.0.0

## Introduction

Simple app to convert RUB to PLN using 3rd party fixer.io API.

## Installation using Composer

```bash
$ composer install
```

Once installed, you can test it out immediately using PHP's built-in web server:

```bash
$ cd path/to/install
$ php -S 0.0.0.0:8080 -t public/ public/index.php
# OR use the composer alias:
$ composer run --timeout 0 serve
```
