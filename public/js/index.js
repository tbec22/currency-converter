jQuery(function($){
    $('form').submit(function(event){
        $('.overlay').show();
        $.ajax({
            method: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data){
                $('.error').remove();
                $('.overlay').hide();

                $.each(data['errors'], function(k, v) {
                        if(k == 'form') {
                            $('.error--form').text(v['general']).show();
                        }
                        else $('input[name='+k+']').parents('.form_element').append('<div class="error">'+Object.values(v)[0]+'</div>');
                });

                $.each(data['data'], function(k, v) {
                    $('input[name='+k+']').val(v);
                });
            },
            error: function() {
                $('.overlay').hide();
                $('.error--form').text('Uuuups! Something went wrong. Try again ...').show();
            }
        })

        event.preventDefault();
        return false;
    })
});