<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Fieldset;
use Zend\Form\Form;
use Zend\Validator;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Math\Rand;
use Zend\Json\Json;
use Zend\Http;
use Zend\Http\Client;
use Zend\Http\Client\Adapter\Socket;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $currency1 = new Element('currency1');
        $currency1->setLabel('RUB: ');
        $currency1->setAttributes(array('type'  => 'text', 'value' => 0,  'autocomplete' => 'off'));

        $currency1input = new Input('currency1');
        $currency1input->getValidatorChain()
        	  ->attach(new Validator\Regex('/^[0-9]+(.[0-9]+)?$/'));
        $currency2 = new Element('currency2');
        $currency2->setLabel('PLN: ');
        $currency2->setAttributes(array('type'  => 'text', 'value' => 0, 'autocomplete' => 'off', 'readonly' => 'readonly'));


        $csrf = new Element\Csrf('security');
        $send = new Element('send');
        $send->setValue('CONVERT');
        $send->setAttributes(array(
            'type'  => 'submit'
        ));

        $form = new Form('contact');
        $form->setAttribute('method', 'post');
        $form->add($currency1);
        $form->add($currency2);
        $form->add($csrf);
        $form->add($send);

        $inputFilter = new InputFilter();
        $inputFilter->add($currency1input);
        $form->setInputFilter($inputFilter);

        $errors = array();

        if($request->isPost()) {
            $form->setData($_POST);

            if ($form->isValid())
            {
                $data = $form->getData();

                $config = array(
                    'adapter'      => '\Zend\Http\Client\Adapter\Socket',
                    'ssltransport' => 'tls'
                );

                try {
                    // Instantiate a client object
                    $client = new \Zend\Http\Client('http://data.fixer.io/api/latest?access_key=bbf710cb6f251e6d68e71745555a4f76&format=1', $config);


                    // The following request will be sent over a TLS secure connection.
                    $apiResponse = $client->send();



                    if ($apiResponse->isSuccess()) {
                        $currencyRate = Json::decode($apiResponse->getBody(), Json::TYPE_ARRAY);
                        $data['currency2'] = ($data['currency1'] * $currencyRate['rates']['PLN']) / $currencyRate['rates']['RUB'];
                    }
                    else {
                        $errors = array('form' => array('general' => 'Can\'t get currency rate'));
                        $data['currency2'] = 0;
                        $currency2->setAttribute('value', 0);
                    }

                } catch (\Throwable $e) {
                    $errors = array('form' => array('general' => 'Uuuups! Something went wrong. Try again ...'));
                }

                $form->setData($data);


                if ($request->isXmlHttpRequest())
                {
                    $response->setContent(Json::encode(array(
                        'status'=>200,
                        'data' => array_intersect_key($data, array_flip(array('currency1','currency2'))),
                        'errors' => $errors
                    )));
                    return $response;
                }
            }
            else
            {
                $data = $_POST;
                $data['currency2'] = 0;
                $currency2->setAttribute('value', 0);

                if ($request->isXmlHttpRequest())
                {
                    $response->setContent(Json::encode(array(
                        'status'=>200,
                        'data' => array_intersect_key($form->getData(), array_flip(array('currency1','currency2'))),
                        'errors' => $form->getMessages()
                    )));
                    return $response;
                }
            }
        }

        return new ViewModel(array(
            'form' => $form,
            'errors' => $errors
        ));
    }
}
